provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "docking_bay" {
  bucket_prefix = "test-drift"

  tags = {
    Name                 = "Test Drift"
    Environment          = "Dev"
    git_commit           = "ab14bb38060530a9e3dc23a399dbc2115931887a"
    git_file             = "terraform/test-drift/s3.tf"
    git_last_modified_at = "2022-06-17 16:17:30"
    git_last_modified_by = "37553582+ziyu-yu@users.noreply.github.com"
    git_modifiers        = "37553582+ziyu-yu"
    git_org              = "ziyu-yu"
    git_repo             = "terragoat-zyu"
    yor_trace            = "d701c93d-6f0b-48ac-ab26-b9e93cb9a440"
  }
}
