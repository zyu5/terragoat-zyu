provider "aws" {
  region = "us-west-2"
}

resource "aws_s3_bucket" "docking_bay" {
  bucket_prefix = "docking-bay-storage-"

  tags = {
    Name                 = "Docking Bay"
    Environment          = "Dev"
    git_commit           = "8c8ecd0bc8eb8c297e605f4c9fd63ce7ab34d915"
    git_file             = "terraform/simple_instance/s3.tf"
    git_last_modified_at = "2022-06-10 03:02:25"
    git_last_modified_by = "37553582+ziyu-yu@users.noreply.github.com"
    git_modifiers        = "37553582+ziyu-yu"
    git_org              = "ziyu-yu"
    git_repo             = "terragoat-zyu"
    yor_trace            = "1aebd0ce-c4e5-46a1-af2b-ea8e8df3327a"
  }
}
